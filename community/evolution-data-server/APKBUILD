# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=evolution-data-server
pkgver=3.37.90
pkgrel=0
pkgdesc="data server for evolution"
url="https://projects.gnome.org/evolution"
arch="all !mips64"
license="GPL-2.0-or-later"
depends_dev="libgdata-dev gcr-dev icu-dev"
makedepends="$depends_dev gperf flex bison glib-dev gtk+3.0-dev libsecret-dev
	libsoup-dev libxml2-dev nss-dev sqlite-dev krb5-dev gnu-libiconv-dev
	openldap-dev json-glib-dev webkit2gtk-dev libgweather-dev
	cmake libical-dev libcanberra-dev vala gobject-introspection-dev
	gnome-online-accounts-dev libphonenumber-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.gnome.org/sources/evolution-data-server/${pkgver%.*}/evolution-data-server-$pkgver.tar.xz"

case "$CARCH" in
	mips*)	options="!check" ;;
esac

build() {
	CFLAGS="$CFLAGS -I/usr/include/gnu-libiconv" \
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DSYSCONF_INSTALL_DIR=/etc \
		-DCMAKE_BUILD_TYPE=None \
		-DENABLE_GOA=ON \
		-DENABLE_UOA=OFF \
		-DENABLE_INTROSPECTION=ON \
		-DENABLE_VALA_BINDINGS=ON \
		-DWITH_SYSTEMUSERUNITDIR=no \
		-DWITH_PHONENUMBER=ON
	cmake --build build
}

check() {
	cd build
	# failing tests on s390x. -locale tests fail due to musl not supporting LC_ADDRESS.
	case "$CARCH" in
		s390x) ;;
		*) CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(test-book-cache-cursor-change-locale|test-sqlite-cursor-change-locale)"
	esac
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="13f92e844ebe2673e0ba10838db9c115dac7d9da18de0281d1d4662d572c01d057a6740b413e5aa9d3361d8dcb3f13f21be996e15cda7cef82f4302a30bccf39  evolution-data-server-3.37.90.tar.xz"
