# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-initial-setup
pkgver=3.37.91
pkgrel=0
pkgdesc="A simple, easy,and safe way to prepare a new system"
url="https://wiki.gnome.org/Design/OS/InitialSetup"
arch="all !armhf !s390x !ppc64le !mips !mips64" # limited by gnome-shell&mutter
license="GPL-2.0-or-later"
depends="gsettings-desktop-schemas iso-codes dbus:org.freedesktop.Secrets"
makedepends="meson cheese-dev ibus-dev networkmanager-dev polkit-dev
	accountsservice-dev gnome-desktop-dev fontconfig-dev libgweather-dev
	gtk+3.0-dev gnome-online-accounts-dev gdm-dev geocode-glib-dev
	geoclue-dev rest-dev libpwquality-dev webkit2gtk-dev libsecret-dev
	libnma-dev krb5-dev"
options="!check" # no tests
subpackages="$pkgname-lang"
source="https://download.gnome.org/sources/gnome-initial-setup/${pkgver%.*}/gnome-initial-setup-$pkgver.tar.xz"

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=plain \
		-Dregion-page=false \
		-Dsoftware-sources=disabled \
		-Dsystemd=false \
		. output
	ninja -C output
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

sha512sums="c6163b8155dc8c4621c2e84d052c021b52a552464782fd0c3520d095495656a401f60af48ab243554f3cb4cd1a5eeff8e1a16dda9c296ba1667c527717feb51f  gnome-initial-setup-3.37.91.tar.xz"
